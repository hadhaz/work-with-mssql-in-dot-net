﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Web.Mvc;
using Moq;
using Ninject;

namespace WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceTypes)
        {
            return kernel.GetService(serviceTypes);
        }

        public IEnumerable<object> GetServices(Type serviceTypes)
        {
            return kernel.GetAll(serviceTypes);
        }

        public void AddBindings()
        {
            Mock<IProduct
        }
    }
}